import { View, Text, Image, DimensionValue, ImageProps } from 'react-native'
import React from 'react'
import styles from './style'

export default function Logo({logoWidth, logoHeight}: any) {
  return (
    <View>
      <Image
        style={{
          width: logoWidth,
          height: logoHeight,
          paddingRight:5,
        }}
        source={require("../../../assets/images/MobileLogoHRManager.png")}
      />
    </View>
  )
}