import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    messageContainer : {
        backgroundColor: "#E7E9EC",
        flexDirection: 'row',
        paddingVertical : 10,
        paddingHorizontal:10,
        justifyContent:'space-between',
    },
    separator:{
        width:15,
        height:2.5,
        backgroundColor:"#0267C1",
        borderRadius:5,
    },
    textContainer:{
        flexDirection:'row',
        alignItems:'center',
        gap: 10,
        paddingLeft: 5,
        
    },
    alignTextAndIcon:{
        flexDirection: 'row',
    }
});

export default styles;