import { View, Text, TouchableOpacity, Pressable } from 'react-native'
import React from 'react'
import HorizontalRow from '../../HorizontalRow/HorizontalRow'
import * as Icons from "react-native-heroicons/solid";
import styles from './style';
import FontAwesome6 from '@expo/vector-icons/FontAwesome6';

export default function MessageListItem({message}: any) {
    

  return (
    <View>
      <Pressable
      onPress={() => {
        
      }}
      >
        <View style={styles.messageContainer}>
            <View style={styles.alignTextAndIcon}>
                <TouchableOpacity>
                    <Icons.UserCircleIcon color="#0267C1" size={34}/>
                </TouchableOpacity>
                <View style={styles.textContainer}>
                    <Text>{message.author}</Text>
                    <View style={styles.separator}></View>
                    <Text>{message.title}</Text>
                </View>
            </View>
            <TouchableOpacity
            onPress={()=> {
                
            }}
            >
                <FontAwesome6 name="trash" size={30} color="#0267C1" />
            </TouchableOpacity>
        </View>
      </Pressable>
    </View>
    
  )
}