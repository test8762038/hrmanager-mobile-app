import { View, Text, FlatList, SafeAreaView } from 'react-native'
import React from 'react'
import MessageListItem from '../MessageListItem/MessageListItem'
import styles from './style'
import HorizontalRow from '../../HorizontalRow/HorizontalRow'

export default function MessageList({messagesList}: any) {
  return (
    <SafeAreaView>
      <FlatList
        style={styles.listContainer}
        data={messagesList}
        ItemSeparatorComponent={() => <HorizontalRow/>}
        renderItem={({ item })=>(
          <MessageListItem message={item}/>
        )}
      />
    </SafeAreaView>
  )
}