import { View, Text, SafeAreaView, TextInput, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import styles from './style'
import { FontAwesome5 } from '@expo/vector-icons';

export default function SearchBar() {
    const [filter, setFilter] = useState<string>("");

  return (
    <View style={styles.searchBarContainer}>
        <View style={styles.borderSearchBarContainer}>
            <TextInput
            style={styles.input}
            onChangeText={setFilter}
            value={filter}
            placeholder='Rechercher '
            />
            <TouchableOpacity style={styles.searchIcon}>
                <FontAwesome5 name="search" size={24} color="#0267C1" />
            </TouchableOpacity>
        </View>
    </View>
  )
}