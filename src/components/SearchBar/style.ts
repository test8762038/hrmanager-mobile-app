import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    searchBarContainer:{
        width:"100%",
        height:68,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:"#FFFFFF"
    },
    borderSearchBarContainer:{
        borderColor:"#0267C1",
        borderWidth:1.5,
        borderRadius:2,
        flexDirection:'row'
    },
    input:{
        width:"60%",
        height:34,
        backgroundColor:"#FFFFFF",
        borderTopRightRadius:0,
        borderTopLeftRadius:2,
        borderBottomLeftRadius:2,
        borderBottomRightRadius:0,
        paddingLeft:5
    },
    searchIcon:{
        width:34,
        height:34,
        backgroundColor:"#FFFFFF",
        justifyContent:'center',
        alignItems:'center',
        elevation:5,
        borderTopRightRadius:2,
        borderTopLeftRadius:0,
        borderBottomLeftRadius:0,
        borderBottomRightRadius:2,
    }
})

export default styles;