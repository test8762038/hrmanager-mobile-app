import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    buttonContainer:{
        backgroundColor:"#FFFFFF",
        width:"100%",
        height:52.65,
    },
    button:{
        backgroundColor:"#0267C1",
        paddingVertical:10,
        alignItems:'center',
    },
    textButton:{
        fontSize:24,
        color:"#FFFFFF",
        fontWeight:"500",
    },
});

export default styles;