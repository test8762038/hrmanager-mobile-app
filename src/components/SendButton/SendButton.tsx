import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './style'

export default function SendButton() {
  return (
    <TouchableOpacity 
    style={styles.buttonContainer}
    onPress={()=>{
      
    }}
    >
      <View style={styles.button}>
        <Text style={styles.textButton}>{'Envoyer un message'}</Text>
      </View>
    </TouchableOpacity>
  )
}