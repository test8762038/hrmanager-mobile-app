import { View, Text, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { Ionicons } from '@expo/vector-icons';
import Logo from '../Logo/Logo';
import { useDrawerStatus } from '@react-navigation/drawer';
import * as Icons from "react-native-heroicons/solid";
import styles from './style';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export default function Header({navigation}:any) {
    const [isDrawerOpen, setDrawerOpening] = useState(false)

    

  return (
    <View style={styles.header}>
        <TouchableOpacity
        onPress={() => {
            navigation.openDrawer();
        }}>
            <Ionicons name="menu" size={34} color="#0267C1" />
        </TouchableOpacity>
        
        <Logo logoWidth={214} logoHeight={38}/>
        <TouchableOpacity
        onPress={() => {
            
        }}>
            <Icons.UserCircleIcon color="#0267C1" size={34}/>
        </TouchableOpacity>
    </View>
  )
}
