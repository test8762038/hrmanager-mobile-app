import { StatusBar, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    header:{
        flexDirection:'row',
        backgroundColor:'#FFFFFF',
        height:70,
        width:"100%",
        paddingTop: StatusBar.currentHeight,
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:10,
        elevation: 20
    }
})

export default styles;