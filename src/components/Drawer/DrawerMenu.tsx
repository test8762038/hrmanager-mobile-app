import React from 'react'
import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from '../../screens/HomeScreen';
import MessagesScreen from '../../screens/MessagesScreen';
import { Ionicons } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';
import { FontAwesome5, FontAwesome, FontAwesome6 } from '@expo/vector-icons';
import Header from '../Header/Header';
import ProjectsScreen from '../../screens/ProjectsScreen';
import TasksScreen from '../../screens/TasksScreen';
import HoursScreen from '../../screens/HoursScreen';
import LeaveRequestsScreen from '../../screens/LeaveRequestsScreen';
import PayslipScreen from '../../screens/PayslipScreen';
import SupportScreen from '../../screens/SupportScreen';
import OptionsScreen from '../../screens/OptionsScreen';


const Drawer = createDrawerNavigator();

export default function DrawerMenu() {
  return (
    
    <Drawer.Navigator
      initialRouteName='Home'
      screenOptions={{
          drawerInactiveBackgroundColor:"#0267C1",
          drawerActiveBackgroundColor:"#FFFFFF",
          drawerActiveTintColor:"#0267C1",
          drawerInactiveTintColor:"#FFFFFF",
          
        header: ({navigation}) => <Header navigation={navigation}/>,
        drawerStyle:{
          width:250,
          backgroundColor:"#0267C1",
        }
      }}
    >
      {/*Page d'accueil */}
      <Drawer.Screen 
        name="Accueil" 
        component={HomeScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome5 name="home" size={24} color={color}/>
          )
      }}/>

      {/*Page de la messagerie */}
      <Drawer.Screen 
        name="Messagerie" 
        component={MessagesScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0,
          },
          drawerIcon: ({color}) => (
            <FontAwesome name="envelope" size={24} color={color} />
          )
      }}/>

        {/*Page des projets */}
      <Drawer.Screen 
        name="Projets" 
        component={ProjectsScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <Octicons name="file-directory" size={24} color={color} />
          )
      }}/>

        {/*Page des tâches */}
        <Drawer.Screen 
        name="Tâches" 
        component={TasksScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome5 name="tasks" size={24} color={color} />
          )
      }}/>

        {/*Page des heures */}
        <Drawer.Screen 
        name="Heures" 
        component={HoursScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome6 name="clock" size={24} color={color} />
          )
      }}/>

        {/*Page des demandes de congés */}
        <Drawer.Screen 
        name="Congés" 
        component={LeaveRequestsScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome6 name="bed" size={24} color={color} />
          )
      }}/>

        {/*Page des fiches de paie */}
        <Drawer.Screen 
        name="Fiche de paie" 
        component={PayslipScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0,
          },
          drawerIcon: ({color}) => (
            <FontAwesome6 name="file-invoice-dollar" size={24} color={color} />
          )
      }}/>

        {/*Page de support */}
        <Drawer.Screen 
        name="Support" 
        component={SupportScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome6 name="screwdriver-wrench" size={24} color={color} />
          )
      }}/>

        {/*Page des options */}
        <Drawer.Screen 
        name="Options" 
        component={OptionsScreen} 
        options={{
          drawerItemStyle:{
            borderColor:"#FFFFFF",
            borderWidth:2,
            borderRadius:0
          },
          drawerIcon: ({color}) => (
            <FontAwesome6 name="gear" size={24} color={color} />
          )
        }}/>
    </Drawer.Navigator>
    
  )
}