// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAepK-mjA1AJY7vqm5KKPjF1oxfxKSHWSI",
  authDomain: "hrmanager-e433c.firebaseapp.com",
  projectId: "hrmanager-e433c",
  storageBucket: "hrmanager-e433c.appspot.com",
  messagingSenderId: "729536439310",
  appId: "1:729536439310:web:b0896794c6eadbb4b7c85f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

const storage = getStorage(app);

export {auth, db, storage}
