export default interface User {
    id: string;
    name: string;
    firstName:string;
    email: string;
}