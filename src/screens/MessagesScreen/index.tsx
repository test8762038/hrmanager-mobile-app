import { View, Text, FlatList, ScrollView } from 'react-native'
import React, { useState } from 'react'
import SearchBar from '../../components/SearchBar/SearchBar'
import styles from './style'
import HorizontalRow from '../../components/HorizontalRow/HorizontalRow';
import MessageList from '../../components/Message/MessageList/MessageList';
import SendButton from '../../components/SendButton/SendButton';

export default function MessagesScreen() {
  const [messages, setMessages] = useState([
    {author: 'Axel GROUSSAUD', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'NON LUS', key: '1'},
    {author: 'Tommy SERAIN', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '2'},
    {author: 'Axel GROUSSAUD', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '3'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'NON LUS', key: '4'},
    {author: 'Tommy SERAIN', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '5'},
    {author: 'Paul KLEIN', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'NON LUS', key: '6'},
    {author: 'Arthur ROUSSEL', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '7'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '8'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '9'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '10'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '11'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '12'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '13'},
    {author: 'Alexandre NOENS', receiver: 'Axel GROUSSAUD', title: 'nouvelle écran', message: 'message 1 écrans', status: 'LUS', key: '14'},
  ]);

  return (
    <View style={{ flex: 1}}>
        <SearchBar/>
        <HorizontalRow/>
        <MessageList messagesList = {messages}/>
        <HorizontalRow/>
        <SendButton/>
    </View>
  )
}