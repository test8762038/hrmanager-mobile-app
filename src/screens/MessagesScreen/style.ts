import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    hr:{
        width:"100%",
        height:5,
        backgroundColor:"#0267C1"
    }
})

export default styles;