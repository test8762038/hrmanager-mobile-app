import { View, Text, Pressable, SafeAreaView, Keyboard, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import { createUserWithEmailAndPassword, onAuthStateChanged } from 'firebase/auth';
import { auth, db } from '../../services/firebaseConfig';
import { doc, setDoc } from 'firebase/firestore';
import styles from './style';
import { FontAwesome5 } from '@expo/vector-icons';

export default function RegisterScreen({navigation}:any) {
  const [name, setName] = useState<string | undefined>();
  const [email, setEmail] = useState<string | undefined>();
  const [password, setPassword] = useState<string | undefined>();


  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        navigation.navigate("Drawer");
        // console.log(user);
      }
    });
  }, []);

  const createUserAndGoToDrawer = async () => {
    // Si email et password on inscrit l'utilisateur dans auth puis dans db
    if (email && password) {
      createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed up
          const user = userCredential.user;
          //Une fois qu'il est créé on l'ajoute à la base de données firestore
          setDoc(doc(db, "users", user.uid), {
            name: name,
            email: email,
            CreatedAt: new Date().toUTCString(),
          });
        })
        .then(() => {
          alert("account created successfully 🎉");
          navigation.navigate("Drawer");
        })
        .catch((err: any) => {
          alert(err.message);
        });
    }
  };


  return (
    <Pressable style={styles.pressableContainer} onPress={Keyboard.dismiss}>
      <SafeAreaView>
        <View style={styles.headerRegisterScreen}>
          <View style={styles.iconContainer}>
            <FontAwesome5 name="user-tie" size={50} color="#0267C1"/>
          </View>
          <View style={styles.interTitle}></View>
          <Text style={styles.title}>{'Créer ton compte'}</Text>
        </View>
        <View style={styles.formContainer}>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>{'Nom'}</Text>

            <TextInput
              style={styles.input}
              onChangeText={setName}
              value={name}
              placeholder="Nom"
              placeholderTextColor="#808080"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>{'Email / Identifiant'}</Text>

            <TextInput
              style={styles.input}
              onChangeText={setEmail}
              value={email}
              placeholder="test@test.com"
              placeholderTextColor="#808080"
              inputMode="email"
              autoCapitalize="none"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>{'Mot de passe'}</Text>

            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              placeholder="******"
              secureTextEntry
              placeholderTextColor="#808080"
            />
          </View>
        </View>
        <View style={styles.buttonList}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.button}
                onPress={createUserAndGoToDrawer}
              >
                <Text style={styles.buttonText}>{`S'inscrire`}</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                <Text style={styles.navigateButton}>{"Revenir à l'écran de connexion"}</Text>
              </TouchableOpacity>
            </View>
          </View>
      </SafeAreaView>
    </Pressable>
  );
}