import { StatusBar, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    pressableContainer:{
        flex:1,
        backgroundColor:"#0267C1",
        width:"100%",
        height:"100%",
    },
    headerRegisterScreen:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:15,
        marginTop:StatusBar.currentHeight,
        paddingVertical:20,
    },
    interTitle:{
        backgroundColor:"#FFFFFF",
        width:20,
        height:5,
        borderRadius:10
    },
    iconContainer:{
        width:70,
        height:70,
        backgroundColor:"#FFFFFF",
        borderRadius:100,
        justifyContent:'center',
        alignItems:'center',
        paddingBottom:5
    },
    title:{
        color:"#FFFFFF",
        fontSize:25,
        fontWeight:"500",
    },
    formContainer:{
        paddingTop:50,
    },
    inputContainer:{
        paddingHorizontal:30,
        gap:5,
    },
    label:{
        color: "#FFFFFF",
        fontWeight:"400",
    },
    input:{
        color: "#0267C1",
        backgroundColor:"#FFFFFF",
        padding: 10,
        fontSize: 18,
        fontWeight:"400",
        marginBottom: 10,
    },
    buttonContainer:{
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        gap:10
    },
    buttonText:{
        fontSize: 20,
        color: '#0267C1',
        fontWeight:"600"
    },
    navigateButton:{
        color: "#FFFFFF",
        fontWeight:"400",
        textDecorationLine: "underline",
    },
    buttonList:{
        paddingTop:90,
        width:"100%",
        height:"100%",
        gap:25,
    },
    button:{
        backgroundColor: "#FFFFFF",
        paddingVertical: 10,
        width:"60%",
        justifyContent: 'center',
        alignItems: "center",
    }
})

export default styles;