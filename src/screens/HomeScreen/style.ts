import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    oneInfoContainer:{
        width:"80%",
        borderColor:"#808080",
        borderWidth:0.5,
        borderRadius:10,
        elevation:5
    },
    title:{
        fontSize:20,
        color:"#0267C1",
        fontWeight:"500",
    },
    info:{
        fontSize:16,
        fontWeight:"400"
    },
    titleContainer:{
        backgroundColor: "#FFFFFF",
        paddingHorizontal: 20,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        paddingVertical:5,
    },
    infoContainer:{
        paddingHorizontal: 20,
        paddingVertical:5,
        backgroundColor: "#E7E9EC",
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
    }
})

export default styles;