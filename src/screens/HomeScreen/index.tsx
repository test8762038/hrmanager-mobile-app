import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './style'

export default function HomeScreen() {
  return (
    <ScrollView>
      <View style={{ flex: 1, paddingVertical: 40, gap:40, alignItems:'center', backgroundColor:"#FFFFFF"}}>
        
          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Projet en cours :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'Application mobile HRManager'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Team actuel :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'Front-end'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Tâche en cours :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'Développer la HomeScreen'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Progression de la tâche en cours :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'60%'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Tâche suivante :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'Développer la MessageScreen'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Nombre de tâches réalisées :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'4'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Nombre de tâches restantes :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'20'}</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.oneInfoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{'Progression du projet en cours :'}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.info}>{'16,7%'}</Text>
            </View>
          </TouchableOpacity>
        
      </View>
    </ScrollView>
  )
}