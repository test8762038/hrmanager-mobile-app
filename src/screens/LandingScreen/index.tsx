import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './style'
import { FontAwesome5, FontAwesome } from '@expo/vector-icons';
import Logo from '../../components/Logo/Logo';
import { Octicons } from '@expo/vector-icons';

export default function LandingScreen({navigation} : any) {
  return (
    <View style={styles.screen}>
        <View style={styles.logoContainer}>
          <Logo logoWidth={284} logoHeight={52}/>
        </View>
        <View style={styles.iconsContainer}>
        <View style={styles.taskIconContainer}>
              <View style={styles.icon}>
                <Octicons name="file-directory" size={28} color="#0267C1" />
              </View>
              <Text style={styles.textIcon}>{'Suivi des projets'}</Text>
          </View>
          <View style={styles.taskIconContainer}>
              <View style={styles.icon}>
                <FontAwesome5 name="tasks" size={28} color="#0267C1" />
              </View>
              <Text style={styles.textIcon}>{'Gestion des tâches'}</Text>
          </View>
          <View style={styles.taskIconContainer}>
              <View style={styles.icon}>
                <FontAwesome name="envelope" size={28} color="#0267C1" />
              </View>
              <Text style={styles.textIcon}>{'Messagerie intégrée'}</Text>
          </View>
          
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{"Bienvenue dans HRManager ! L'application qui gère vos projets, vos tâches et bien plus encore !"}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity 
          style={styles.button}
          onPress={() => navigation.navigate("Login")}
          >
            <Text style={styles.textButton}>{'Démarrer'}</Text>
          </TouchableOpacity>
        </View>
    </View>
  )
}