import { StatusBar, StyleSheet } from "react-native";

const styles = StyleSheet.create({
screen:{
    flex:1, 
    backgroundColor: "#FFFFFF",
    width:"100%", 
    height:"100%",
},
iconsContainer:{
    backgroundColor:"#0267C1",
    height:"40%",
    flexDirection:'row',
    justifyContent:'center',
    gap:5,
},
logoContainer: {
    width:"100%",
    marginTop:StatusBar.currentHeight,
    paddingHorizontal:40,
    paddingVertical:10
},
taskIconContainer:{
    width:"30%",
    justifyContent:'center',
    height:"100%",
    alignItems:'center',
    gap:20
},
icon:{
    borderRadius:100,
    backgroundColor:"#FFFFFF",
    width:60,
    height:60,
    alignItems:'center',
    justifyContent:'center'
    
},
textIcon:{
    fontSize:18,
    textAlign:'center',
    color:"#FFFFFF",
    fontWeight:"600"
},
description:{
    color:"#0267C1",
    fontSize:16,
    fontWeight:"600"
},
descriptionContainer:{
    height:"20%",
    justifyContent:'center',
    alignItems:'center',
    paddingHorizontal:20
},
buttonContainer:{
    backgroundColor:"#FFFFFF",
    paddingHorizontal:60,
    paddingVertical:30
},
button:{
    backgroundColor:"#0267C1",
    paddingVertical:10,
    alignItems:'center'
},
textButton:{
    fontSize:24,
    color:"#FFFFFF",
    fontWeight:"500"
}
})

export default styles;