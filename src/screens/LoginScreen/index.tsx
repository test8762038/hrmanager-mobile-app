import { View, Text, SafeAreaView, Pressable, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import { FontAwesome5 } from '@expo/vector-icons';
import styles from './style';
import { auth } from '../../services/firebaseConfig';
import { onAuthStateChanged, signInWithEmailAndPassword } from 'firebase/auth';

export default function LoginScreen({navigation} : any) {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        navigation.navigate("Drawer");
        // console.log(user);
      }
    });
  }, []);

  const loginAndGoToDrawer = async () => {
    if (email.length > 0 && password.length > 0) {
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          navigation.navigate("Drawer");
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
        });
    }
  };

  
  return (
    <Pressable style={styles.loginContainer}>
      <SafeAreaView>
        <View style={styles.headerLoginScreen}>
          <View style={styles.iconContainer}>
            <FontAwesome5 name="user-tie" size={100} color="#0267C1"/>
          </View>
          <Text style={styles.title}>{'Connexion'}</Text>
        </View>
        <View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>{'Email / Identifiant'}</Text>

            <TextInput
              style={styles.input}
              onChangeText={setEmail}
              value={email}
              placeholder="test@test.com"
              placeholderTextColor="#808080"
              inputMode="email"
              autoCapitalize="none"
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.label}>{'Mot de passe'}</Text>

            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              placeholder="******"
              secureTextEntry
              placeholderTextColor="#808080"
            />
          </View>
          <View style={styles.buttonList}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.button}
                onPress={loginAndGoToDrawer}
              >
                <Text style={styles.buttonText}>{`Se connecter`}</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                <Text style={styles.navigateButton}>{"S'inscrire"}</Text>
              </TouchableOpacity>
              <Text style={{color:"#FFFFFF"}}>{'|'}</Text>
              <TouchableOpacity onPress={() => navigation.navigate("Drawer")}>
                <Text style={styles.navigateButton}>{"Continuer en tant qu'invité"}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Pressable>
  )
}