import { StatusBar, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    loginContainer:{
        flex:1,
        width:"100%",
        height:"100%",
        backgroundColor:"#0267C1"
    },
    headerLoginScreen:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:StatusBar.currentHeight,
        paddingVertical:60
    },
    iconContainer:{
        backgroundColor:"#FFFFFF",
        borderRadius:100,
        width:140,
        height:140,
        justifyContent:'center',
        alignItems:'center',
        paddingBottom:10
    },
    title:{
        color:"#FFFFFF",
        fontSize:35,
        fontWeight:"500"
    },
    label:{
        color: "#FFFFFF",
        fontWeight:"400",
        width:"100%"
    },
    input: {
        color: "#0267C1",
        backgroundColor:"#FFFFFF",
        height: 40,
        padding: 10,
        fontSize: 18,
        fontWeight:"400",
        marginBottom: 10,
        width:"100%"
    },
    button: {
        backgroundColor: "#FFFFFF",
        paddingVertical: 10,
        width:"60%",
        justifyContent: 'center',
        alignItems: "center",
    },
    buttonText: {
        fontSize: 20,
        color: '#0267C1',
        fontWeight:"600"
    },
    navigateButton: {
        color: "#FFFFFF",
        fontWeight:"400",
        textDecorationLine: "underline",
    },
    inputContainer:{
        paddingHorizontal:30,
        gap:5
    },
    buttonContainer:{
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        gap:10
    },
    buttonList:{
        paddingTop:50,
        gap:25,
    }

})

export default styles;