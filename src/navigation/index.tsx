import { View, Text } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import LandingScreen from '../screens/LandingScreen';
import LoginScreen from '../screens/LoginScreen';
import DrawerMenu from '../components/Drawer/DrawerMenu';
import RegisterScreen from '../screens/RegisterScreen';
import SendMessageScreen from '../screens/SendMessageScreen';
import ShowMessageScreen from '../screens/ShowMessageScreen';

const Stack = createStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        detachInactiveScreens
        screenOptions={{ gestureEnabled: false }}
      >
        <Stack.Screen
          name="Landing"
          component={LandingScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Drawer"
          component={DrawerMenu}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="SendMessage"
          component={SendMessageScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ShowMessage"
          component={ShowMessageScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>

  )
}