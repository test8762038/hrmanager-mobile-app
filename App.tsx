import {
  useFonts,
  LeagueSpartan_100Thin,
  LeagueSpartan_200ExtraLight,
  LeagueSpartan_300Light,
  LeagueSpartan_400Regular,
  LeagueSpartan_500Medium,
  LeagueSpartan_600SemiBold,
  LeagueSpartan_700Bold,
  LeagueSpartan_800ExtraBold,
  LeagueSpartan_900Black,
} from '@expo-google-fonts/league-spartan';

import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';

import DrawerMenu from './src/components/Drawer/DrawerMenu';
import Navigation from './src/navigation';

export default function App() {
  
  return (
    
    <SafeAreaView style={{flex:1}}>
      <Navigation/>
    </SafeAreaView>
  );
}


